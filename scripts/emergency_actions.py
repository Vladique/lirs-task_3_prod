#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist
from actionlib_msgs.msg import GoalStatusArray
from sensor_msgs.msg import LaserScan

from time import sleep



global current_goal
current_goal = PoseStamped()

global stuck
stuck = False

global obstacle_is_right
obstacle_is_right = True

global reached
reached = False

global second_goal_sent
second_goal_sent = False


def pose_listener(msg):
	global current_goal
	current_goal.header = msg.header
	current_goal.pose = msg.pose
	
	
def status_listener(msg):
	global stuck
	global reached
	if msg.status_list != []:
		text = msg.status_list[0].text
		stuck = (text == 'Failed to find a valid control. Even after executing recovery behaviors.')
		reached = (text == 'Goal reached.')
			
		
def laser_listener(msg):
	global obstacle_is_right
	obstacle_is_right = msg.ranges[585] < 1
	
		

def rotate():
	global obstacle_is_right
	pub_cmd = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
	vel = Twist()
	pub_goal = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
	
	rospy.loginfo('looks like im stuck!')
	t0 = rospy.Time.now().to_sec()
	while (rospy.Time.now().to_sec() - t0 < 2) and not rospy.is_shutdown():
		if obstacle_is_right:
			vel.angular.z = 1
			pub_cmd.publish(vel)
		else:
			vel.angular.z = -1
			pub_cmd.publish(vel)
		

	rospy.loginfo('done!')
	rospy.loginfo('publishing goal...')
	pub_goal.publish(current_goal)
	rospy.loginfo('done!')
	
	

def main():
	rospy.init_node('test')
	
	sub_goal = rospy.Subscriber('/move_base_simple/goal', PoseStamped, pose_listener)
	sub_status = rospy.Subscriber('/move_base/status', GoalStatusArray, status_listener)
	sub_laser = rospy.Subscriber('/front/scan', LaserScan, laser_listener)
	
	pose = PoseStamped()
	pose.header.frame_id = 'odom'
	pose.pose.position.x = 26.8
	pose.pose.position.y = 41.2
	pose.pose.position.z = 0
	pose.pose.orientation.w = 0.649
	goal_pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=1)
	sleep(2)
	goal_pub.publish(pose)
	
	rate = rospy.Rate(1)
	
	global current_goal
	global stuck
	global reached
	global second_goal_sent
	
	t0_1 = rospy.Time.now().to_sec()
	
	while not rospy.is_shutdown():
		if stuck and rospy.Time.now().to_sec() - t0_1 > 4:
			rotate()
			t0_1 = rospy.Time.now().to_sec()
		if reached and not second_goal_sent:
			print('reached. sending one more')
			pose.pose.position.x = 33.310
			pose.pose.position.y = 91.480
			pose.pose.position.z = 0
			pose.pose.orientation.w = 0.649
			goal_pub.publish(pose)
			second_goal_sent = True
		rate.sleep()




if __name__ == '__main__':
	try:
        	main()
	except rospy.ROSInterruptException:
	        pass
